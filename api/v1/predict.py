import json
import pickle
import traceback
import numpy as np
from flask import Blueprint, request, jsonify
from utils import generate_id, Logger, Response, Decorators, process_name
from config import MODEL_PATH



api = Blueprint('api', __name__, url_prefix='/api/v1')
ml_model = pickle.load(open(MODEL_PATH, 'rb'))

@api.route('/fraud-name-check', methods=['POST'])
@Decorators.required_params("name")
def fraud_name_check():
	logger = Logger(generate_id()).event("FRAUD-NAME-CHECK")
	try:
		params = json.loads(request.data.decode('utf-8'))
	except Exception as e:
		msg = "Malformed JSON"
		logger.error(msg)
		traceback.print_exc()
		return jsonify(code="01", msg=msg)

	try:
		name = params.get('name')
		if name.strip() == '':
			msg = "Invalid name"
			logger.info(msg)
			return jsonify(code="01", msg=msg)

		logger.info(f"Processing name: {name}")
		processed_name = process_name(name)
		logger.info(f"Processed name: {processed_name}")
		
		prediction = ml_model.predict(np.array([processed_name]))[0]
		logger.info(f"Model prediction: {prediction}")
		prediction = True if prediction == True else False
		return jsonify(code="00", msg="Successful", data={'is_fraud': prediction})

	except Exception as e:
		msg = "An error occurred"
		logger.error(msg)
		traceback.print_exc()
		return jsonify(code="01", msg=msg)
