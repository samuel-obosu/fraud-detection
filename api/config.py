import os

class Config(object):
	DEBUG = False


class Development(Config):
	DEBUG = True
	SECRET_KEY = "3#\xf5\xa8\xb9\xb8\xe5\xfb\x05\xabC!\x9a\x95\xeb\xd1NF\x05\xfd\xf0\x91;a"

class Production(Config):
	DEBUG = False
	SECRET_KEY = os.environ.get('SECRET_KEY', "3#\xf5\xa8\xb9\xb8\xe5\xfb\x05\xabC!\x9a\x95\xeb\xd1NF\x05\xfd\xf0\x91;a")


CURR_DIR =  os.path.dirname(__file__)
MODEL_PATH = os.path.join(CURR_DIR, "../trained_models/fraud_name_prediction/hard_ensemble.sav")
FEATURE_WEIGHTS = {'vowels': 1, 'whitespace':2, 'consonants':10, 'numbers':20}
